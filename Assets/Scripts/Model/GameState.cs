﻿namespace Tactile.TactileMatch3Challenge.Model {

    public class GameState {

        internal int movesUsed {
            get;
            private set;
        }

        internal int movesLeft {
            get;
            private set;
        }

        internal int requiredObjectiveCount {
            get;
            private set;
        }

        internal int objectiveType {
            get;
            private set;
        }

        public GameState(int movesLeft, int objectiveCount, int objectiveType) {
            movesUsed = 0;
            this.movesLeft = movesLeft;
            this.requiredObjectiveCount = objectiveCount;
            this.objectiveType = objectiveType;
        }

        public void AddObjective(int count) {
            requiredObjectiveCount -= count;
        }

        public void UseMove() {
            if (movesLeft <= 0) {
                return;
            }
            ++movesUsed;
            --movesLeft;
        }

        internal bool IsFinished() {
            return GameWon() || movesLeft <= 0;
        }

        internal bool GameWon() {
            return requiredObjectiveCount <= 0;
        }
    }
}