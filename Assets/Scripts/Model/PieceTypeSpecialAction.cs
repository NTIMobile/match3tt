﻿using UnityEngine;
using System;

namespace Tactile.TactileMatch3Challenge.Model {

    [Serializable]
    public class PieceTypeSpecialAction {

        [SerializeField]
        internal int pieceTypeIndex = 0;

        [SerializeField]
        internal ESpecialActions SpecialAction;
    }
}