namespace Tactile.TactileMatch3Challenge.Model {

	public interface IPieceSpawner {
		
		int CreateBasicPiece();

		int CreatePowerPiece();

		ESpecialActions GetSpecialActionForPieceType(int pieceType);

		void InitializePowerPieces(PowerPieceActions powerPiecesActions);

		void InitializePieceTypeDatabase(PieceTypeDatabase pieceTypeDatabase);
	}

}