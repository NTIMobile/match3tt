﻿using System.Collections.Generic;
using UnityEngine;

namespace Tactile.TactileMatch3Challenge.Model {

    public class PowerPieceActions : MonoBehaviour {
        [SerializeField] internal List<PieceTypeSpecialAction> specialActionsPerPieceTypeId;

        internal ESpecialActions GetSpecialActionForPieceType(int pieceType) {
            PieceTypeSpecialAction foundMatch = specialActionsPerPieceTypeId.Find(match => match.pieceTypeIndex.Equals(pieceType));
            if (foundMatch != null)
                return foundMatch.SpecialAction;
            return ESpecialActions.None;
        }

        internal int GetRandomPowerPieceType() {
            return specialActionsPerPieceTypeId != null && specialActionsPerPieceTypeId.Count>0 ? 
                specialActionsPerPieceTypeId[UnityEngine.Random.Range(0, specialActionsPerPieceTypeId.Count)].pieceTypeIndex :
                -1;
        }

    }

    public enum ESpecialActions {
        None,
        RemoveColumn,
        RemoveRow
    }
}