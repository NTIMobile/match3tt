using System.Collections.Generic;

namespace Tactile.TactileMatch3Challenge.Model {

	public class PieceSpawner : IPieceSpawner {

        private List<int> basicPieceTypes;

        private PowerPieceActions powerPiecesActions;

        public PieceSpawner(){
            basicPieceTypes = new List<int>();
        }

        public int CreateBasicPiece() {
			return UnityEngine.Random.Range(0, basicPieceTypes.Count);
		}

        public int CreatePowerPiece(){
            return powerPiecesActions.GetRandomPowerPieceType();
        }

        public ESpecialActions GetSpecialActionForPieceType(int pieceType){
            return powerPiecesActions != null ? powerPiecesActions.GetSpecialActionForPieceType(pieceType) : ESpecialActions.None;
        }

        public void InitializePowerPieces(PowerPieceActions powerPiecesActions){
            this.powerPiecesActions = powerPiecesActions;
        }

        public void InitializePieceTypeDatabase(PieceTypeDatabase pieceTypeDatabase){
            basicPieceTypes = new List<int>();

            for (int i = 0; i < pieceTypeDatabase.GetPieceCount(); i++){
                basicPieceTypes.Add(i);
            }

            if (powerPiecesActions != null){
                powerPiecesActions.specialActionsPerPieceTypeId.ForEach(match => basicPieceTypes.Remove(match.pieceTypeIndex));
            }
        }

    }

}