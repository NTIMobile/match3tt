﻿using Tactile.TactileMatch3Challenge.Model;
using Tactile.TactileMatch3Challenge.ViewComponents;
using Tactile.TactileMatch3Challenge.ViewComponents.UI;
using UnityEngine;

namespace Tactile.TactileMatch3Challenge {
	
	public class Boot : MonoBehaviour {

		[SerializeField] private PowerPieceActions powerPiecesActions;

		[SerializeField] private PieceTypeDatabase pieceTypeDatabase;

		[SerializeField] private BoardRenderer boardRenderer;

		[SerializeField] private MainMenuView mainMenuView;

		[SerializeField] private GameView gameView;

		private Board currentBoard;

        public void StartNewGame () {

			int[,] boardDefinition = {
				{3, 3, 1, 2, 3, 3},
				{2, 2, 1, 2, 3, 3},
				{1, 1, 0, 0, 2, 2},
				{2, 2, 0, 0, 1, 1},
				{1, 1, 2, 2, 1, 1},
				{1, 1, 2, 2, 1, 1},
			};

			var pieceSpawner = new PieceSpawner();
			pieceSpawner.InitializePowerPieces(powerPiecesActions);
			pieceSpawner.InitializePieceTypeDatabase(pieceTypeDatabase);

			currentBoard = Board.Create(boardDefinition, pieceSpawner);

			gameView.Initialize(currentBoard);
			mainMenuView.Initialize(currentBoard);
			boardRenderer.Initialize(currentBoard);

			currentBoard.StartGameplay();
		}

		public void FinishGame() {
			currentBoard.FinishGameplay();
			boardRenderer.DestroyVisualPieces();

		}

	}

}
