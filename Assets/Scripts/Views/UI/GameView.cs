﻿using Tactile.TactileMatch3Challenge.Model;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Tactile.TactileMatch3Challenge.ViewComponents.UI {

    public class GameView : UIView {

        [SerializeField] private Canvas scoreViewCanvas;

        [SerializeField] private TextMeshProUGUI scoreText;

        [SerializeField] private TextMeshProUGUI movesCounterText;

        [SerializeField] private TextMeshProUGUI objectiveCountText;

        [SerializeField] private Image objectiveTypeImage;

        [SerializeField] private PieceTypeDatabase pieceTypeDatabase;

        override public void Initialize(Board board) {
            base.Initialize(board);

            if (board == null) {
                return;
            }
            scoreViewCanvas.enabled = false;
        }

        override public void UpdateUI(GameState gameplayState){
            viewCanvas.enabled = gameplayState != null;
            scoreViewCanvas.enabled = false;

            if (gameplayState == null){
                objectiveTypeImage.sprite = null;
                return;
            }

            if (objectiveTypeImage.sprite == null) {
                objectiveTypeImage.sprite = pieceTypeDatabase.GetSpriteForPieceType(gameplayState.objectiveType);
            }

            if (gameplayState.IsFinished()){
                scoreViewCanvas.enabled = true;
                scoreText?.SetText($"{(gameplayState.GameWon()?"Congratulations! You Won!":"Oooops, you lost...")}");
            }

            movesCounterText?.SetText($"Moves: {gameplayState.movesUsed}\nLeft: {gameplayState.movesLeft}");
            objectiveCountText?.SetText($"{(gameplayState.requiredObjectiveCount<=0?"Good!":$"x {gameplayState.requiredObjectiveCount} to win!")}");
        }

    }
}
