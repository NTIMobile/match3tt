﻿using Tactile.TactileMatch3Challenge.Model;
using UnityEngine;

namespace Tactile.TactileMatch3Challenge.ViewComponents.UI {

    public abstract class UIView : MonoBehaviour {

        [SerializeField] internal Canvas viewCanvas;

        virtual public void Initialize(Board board){
            if (board != null) {
                board.OnGameStateChanged += UpdateUI;
            }
        }

        virtual public void UpdateUI(GameState gameplayState) {
        }
    }
}