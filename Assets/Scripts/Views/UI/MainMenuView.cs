﻿using Tactile.TactileMatch3Challenge.Model;

namespace Tactile.TactileMatch3Challenge.ViewComponents.UI {

    public class MainMenuView : UIView {

        override public void Initialize(Board board) {
            base.Initialize(board);

            if (board == null) {
                viewCanvas.enabled = true;
            }
        }

        override public void UpdateUI(GameState gameplayState) {
            viewCanvas.enabled = gameplayState == null;
        }

    }
}