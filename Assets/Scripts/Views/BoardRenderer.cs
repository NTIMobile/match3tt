﻿using System.Collections.Generic;
using Tactile.TactileMatch3Challenge.Model;
using UnityEngine;

namespace Tactile.TactileMatch3Challenge.ViewComponents {

	public class BoardRenderer : MonoBehaviour {
		
		[SerializeField] private PieceTypeDatabase pieceTypeDatabase;
		[SerializeField] private VisualPiece visualPiecePrefab;
		
		private Board board;
		private bool interactionActive = true;
		private List<VisualPiece> currentlyAnimating;

		public void Initialize(Board board) {
			this.board = board;
			interactionActive = true;
			currentlyAnimating = new List<VisualPiece>();

			CenterCamera();
			CreateVisualPiecesFromBoardState(true);
		}

		private void CenterCamera() {
			Camera.main.transform.position = new Vector3((board.Width-1)*0.5f,-(board.Height-1)*0.5f);
		}

		private void CreateVisualPiecesFromBoardState(bool isInitial = false) {
			DestroyVisualPieces();

			foreach (var pieceInfo in board.IteratePieces()) {
				
				var visualPiece = CreateVisualPiece(pieceInfo.piece);
				visualPiece.transform.localPosition = LogicPosToVisualPos(pieceInfo.pos.x, pieceInfo.pos.y);

				if (!isInitial){
					int movementForPiece = board.RequiresFallAnimation(pieceInfo.piece);

					if (movementForPiece != 0) {
						interactionActive = false;
						currentlyAnimating.Add(visualPiece);
						visualPiece.AnimateFall(movementForPiece, NoLongerAnimates);
					}
				}
			}
		}

        private void NoLongerAnimates(VisualPiece visualPiece) {
			currentlyAnimating.Remove(visualPiece);
			interactionActive = currentlyAnimating.Count == 0;
		}

        public Vector3 LogicPosToVisualPos(float x,float y) { 
			return new Vector3(x, -y, -y);
		}

		private BoardPos ScreenPosToLogicPos(float x, float y) { 
			
			var worldPos = Camera.main.ScreenToWorldPoint(new Vector3(x,y,-Camera.main.transform.position.z));
			var boardSpace = transform.InverseTransformPoint(worldPos);

			return new BoardPos() {
				x = Mathf.RoundToInt(boardSpace.x),
				y = -Mathf.RoundToInt(boardSpace.y)
			};

		}

		private VisualPiece CreateVisualPiece(Piece piece) {

			var pieceObject = Instantiate(visualPiecePrefab, transform, true);
			var sprite = pieceTypeDatabase.GetSpriteForPieceType(piece.type);
			pieceObject.SetSprite(sprite);
			return pieceObject;
		}

		internal void DestroyVisualPieces() {
			foreach (var visualPiece in GetComponentsInChildren<VisualPiece>()) {
				Object.Destroy(visualPiece.gameObject);
			}
		}

		private void Update() {
			
			if (board!= null && board.gameplayState != null && interactionActive && Input.GetMouseButtonDown(0)) {

				var pos = ScreenPosToLogicPos(Input.mousePosition.x, Input.mousePosition.y);

				if (board.IsWithinBounds(pos.x, pos.y) && board.Resolve(pos.x, pos.y)!= null) {
					CreateVisualPiecesFromBoardState();
				}

			}
		}
		
	}

}
