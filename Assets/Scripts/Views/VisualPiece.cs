using DG.Tweening;
using System;
using UnityEngine;

namespace Tactile.TactileMatch3Challenge.ViewComponents {
    
    [RequireComponent(typeof(SpriteRenderer))]
    public class VisualPiece : MonoBehaviour {

        [SerializeField]
        private float fallAnimTimeSeconds = 0.345f;

        private Transform mTransform;

        private int lastMovementDiffY = 0;

        private Action<VisualPiece> onAnimationCompleteAction;

        private void Awake() {
            mTransform = transform;
        }

        public void SetSprite(Sprite sprite) {
            var spriteRenderer = GetComponent<SpriteRenderer>();
            spriteRenderer.sprite = sprite;
        }

        internal void AnimateFall(int animateMovementY, Action<VisualPiece> onAnimationCompleteAction) {

            if (animateMovementY == 0 || mTransform==null) {
                return;
            }

            lastMovementDiffY = animateMovementY;
            this.onAnimationCompleteAction = onAnimationCompleteAction;

            mTransform.DOMoveY(mTransform.position.y + lastMovementDiffY, 0).onComplete += MovePieceToPlace;
        }

        private void MovePieceToPlace() {

            if (mTransform == null) {
                return;
            }

            mTransform.DOMoveY(mTransform.position.y - lastMovementDiffY, fallAnimTimeSeconds).onComplete += () => onAnimationCompleteAction.Invoke(this);
        }
    }
}